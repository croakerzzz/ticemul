package com.es.tictest.emul;

import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

import java.util.ArrayList;

/**
 * Created by fezzz on 11.08.2016.
 */
public class Main {
    private static Logger logger = Logger.getLogger(Main.class);

    static public void main(String args[]) {
        try {
            PatternLayout layout = new PatternLayout("%d %-5p %t %m%n");

            ConsoleAppender appender = new ConsoleAppender(layout);
            logger.addAppender(appender);

            DailyRollingFileAppender appender1 =
                new DailyRollingFileAppender(layout, "ticemul.log", "'.'yyyy-MM-dd");
            logger.addAppender(appender1);

            for (long i = 0; i < Integer.valueOf(args[0]); i++) {
                (new Work(i + 1, Integer.valueOf(args[1]))).start();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static class Work extends Thread {
        private long id;
        private int count;

        public Work(long id, int count) {
            this.id = id;
            this.count = count;
        }

        public void run() {
            try {
                while (count > 0) {
                    Gson gson = new Gson();

                    HttpClient client = HttpClientBuilder.create().build();

                    ArrayList<Data> list = new ArrayList<>();
                    list.add(new Data(1, 0, 0, 0, 4.0, 0, 0, 5, "GPS", 1393337635000l));
                    list.add(new Data(1, 0, 0, 0, 4.0, 0, 0, 5, "GPS", 1393337635000l));
                    list.add(new Data(1, 0, 0, 0, 4.0, 0, 0, 5, "GPS", 1393337635000l));
                    list.add(new Data(1, 0, 0, 0, 4.0, 0, 0, 5, "GPS", 1393337635000l));

                    HttpPost httpPost = new HttpPost("http://localhost:8080/tictest/test");
                    StringEntity data = new StringEntity(gson.toJson(list));
                    httpPost.addHeader("content-type", "application/json");
                    httpPost.setEntity(data);
                    HttpResponse response = client.execute(httpPost);

                    count--;

                    logger.info("response_code: " + response.getStatusLine().getStatusCode() + ", count: " + count);

                    if (count > 0) {
                        Thread.sleep(5000);
                    }
                }
            } catch (Exception ex) {
                logger.error(ex);
            }
        }
    }

    private static class Data {
        long trackerId;
        double latitude;
        double longitude;
        double horizontalAccuracy;
        double speed;
        double course;
        double altitude;
        double verticalAccuracy;
        String actuality;
        long timestamp;

        public Data(long trackerId, double latitude, double longitude, double horizontalAccuracy, double speed,
                    double course,
                    double altitude, double verticalAccuracy, String actuality, long timestamp) {
            this.trackerId = trackerId;
            this.latitude = latitude;
            this.longitude = longitude;
            this.horizontalAccuracy = horizontalAccuracy;
            this.speed = speed;
            this.course = course;
            this.altitude = altitude;
            this.verticalAccuracy = verticalAccuracy;
            this.actuality = actuality;
            this.timestamp = timestamp;
        }
    }
}
